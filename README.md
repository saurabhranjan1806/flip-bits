# Flip Bits

## Requirements:
node version >= 12.0.0

## Steps to run:
1. Get into the project root directory where package.json sits
2. ``` yarn install ```
3. ``` yarn start ```
