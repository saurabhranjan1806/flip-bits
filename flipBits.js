/**
 * @author Saurabh Ranjan
 * importing modules
 */
const Promise = require('bluebird');
const readline = require('readline');

/**
 * Initial configuraitons
 */
const initial = [];
const target = [];
let yourFlips = 0;
const goalFlips = 20;
const reader = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});
const CHARSTRING = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

/**
 * returns a random number both max, min inclusive
 * @param {number} max - max limit of the random
 * @param {number} min - min limit, by default 0
 */
const getRandomNumber = (max, min = 0) => Math.round(Math.random() * (max - min)) + min;

/**
 * this function asks question for you
 * @param {string} question - question to print in console
 */
const getInput = question =>
  new Promise((resolve, reject) => {
    reader.question(question, (answer) => {
      resolve(answer);
    });
  });

/**
 * prints the current situation of the game.
 */
const printCurrentSituation = () => {
  console.log(`\nYour Flips: ${yourFlips} | Goal: ${goalFlips}\n`);

  for (let i = 0; i < initial.length; i += 1) {
    if (i === 0) {
      let boardTitle = '--Board';
      let goalTitle = '--Goal';
      let boardCols = '    ';
      let goalCols = '';
      initial.forEach((el, idx) => {
        boardTitle = boardTitle += '--';
        goalTitle = goalTitle += '--';
        boardCols = boardCols += `${CHARSTRING[idx]} `;
        goalCols = goalCols += `${CHARSTRING[idx]} `;
      });
      if (initial.length > 2) {
        boardTitle = boardTitle.substring(0, boardTitle.length - 4);
        goalTitle = goalTitle.substring(0, goalTitle.length - 6);
      } else {
        boardTitle = boardTitle.substring(0, 7);
        goalTitle = goalTitle.substring(0, 6);
      }
      console.log(`${boardTitle} ${goalTitle}\n`);
      console.log(`${boardCols}${goalCols}\n`);
    }
    const spacer = i + 1 > 9 ? ' ' : '  ';
    console.log(`${i + 1}.${spacer}${initial[i]} ${target[i]}`);
  }
  console.log('\n');
};

/**
 * checks if the initial and target arrays match or not
 */
const checkForMatch = () => {
  let isMatch = true;
  for (let i = 0; i < initial.length; i += 1) {
    for (let j = 0; j < initial[i].length; j += 1) {
      if (initial[i][j] !== target[i][j]) {
        isMatch = false;
        return isMatch;
      }
    }
  }
  return isMatch;
};

/**
 * this function flip the given row or a column at once
 * @param {boolean} isRowFlip - tells whether a row flip or a column flip
 * @param {int} indexToBeFlipped - index of the row or column to be flipped
 */
const flipRowOrColumnInitialArray = (isRowFlip, indexToBeFlipped) => {
  if (isRowFlip) {
    const row = [];
    initial[indexToBeFlipped].forEach((i) => {
      row.push(i === 1 ? 0 : 1);
    });
    initial[indexToBeFlipped] = row;
  } else {
    // column flip
    initial.forEach((row) => {
      row[indexToBeFlipped] = row[indexToBeFlipped] === 1 ? 0 : 1;
    });
  }
};

/**
 * This function takes the input for the size of the array.
 */
const startFlipBitsGame = () =>
  getInput('Enter the size of the flip array(NxN) Enter N: ')
    .then((answer) => {
      if (parseInt(answer) && parseInt(answer) <= 26) {
        console.log(
          `generating a random state for ${answer}x${answer} flip array`
        );
        return answer;
      }
      console.log('Please enter a number between 1 and 26.');
      return startFlipBitsGame();
    });

/**
 * This function creates the target configuration
 * @param {number} size - This is the size of the 2-d matrix array.
 */
const initTargetAndInitialFlipArrays = size => new Promise((resolve, reject) => {
  for (let i = 0; i < size; i += 1) {
    const row = [];
    for (let j = 0; j < size; j += 1) {
      row.push(getRandomNumber(1));
    }
    target.push([ ...row ]);
    initial.push([ ...row ]);
  }
  // console.log({ target });
  resolve();
});

/**
 * This function creates the initial configuration
 */
const setInitialFlipArray = () => new Promise((resolve, reject) => {
  const numberOfFlipsToPerform =
    initial.length === 1 ? 1 : getRandomNumber(20, 1);
  // console.log({ numberOfFlipsToPerform });
  let lastRowOperation,
    currentRowOperation,
    lastColOperation,
    currentColOperation;
  for (let i = 0; i < numberOfFlipsToPerform; i += 1) {
    /**
       * to verify initial and target are not equal
       * no last row or column operation can be same to ensure this condition
       */
    let isRowFlip, indexToBeFlipped;
    while (
      lastRowOperation === currentRowOperation ||
        lastColOperation === currentColOperation
    ) {
      // console.log({ lastOperation, currentOperation });
      isRowFlip = getRandomNumber(1);
      indexToBeFlipped = getRandomNumber(initial.length - 1);
      if (isRowFlip) {
        currentRowOperation = `R${indexToBeFlipped}`;
      } else {
        currentColOperation = `C${indexToBeFlipped}`;
      }
      // console.log({ isRowFlip, indexToBeFlipped, currentRowOperation, currentColOperation });
    }
    // flipping the array with currentOperation
    flipRowOrColumnInitialArray(isRowFlip, indexToBeFlipped);
    lastRowOperation = currentRowOperation;
    lastColOperation = currentColOperation;
  }
  // console.log({ initial });
  printCurrentSituation();
  resolve();
});

/**
 * this function is resposible to make a move in the game
 * it then checks for the result after the move
 */
const giveAMove = () =>
  getInput('Press line or column to flip: ')
    .then((answer) => {
      let isRowFlip = true;
      let index;
      if (parseInt(answer) && parseInt(answer) <= initial.length) {
        isRowFlip = true;
        index = parseInt(answer) - 1;
      } else if (answer.length === 1 && CHARSTRING.indexOf(answer.toUpperCase()) > -1) {
        isRowFlip = false;
        index = CHARSTRING.indexOf(answer.toUpperCase());
      } else {
        console.log(
          'Please enter a number between 1 and 26 or a character between A to Z.'
        );
        return giveAMove();
      }
      return { isRowFlip, index };
    })
    .then(({ isRowFlip, index }) => {
      flipRowOrColumnInitialArray(isRowFlip, index);
      yourFlips += 1;
      printCurrentSituation();
    })
    .then(() => {
      // check if the results match or moves left
      if (yourFlips >= goalFlips) {
        return 'All moves exausted! You Lost :(\n';
      }
      if (checkForMatch()) {
        return `Wow! You won 🥳\nMoves taken: ${yourFlips}\nNice game!\n`;
      }
      return giveAMove();
    });

/**
 * this is the starting point of the application
 * 1. We setup the game by asking the size the array
 * 2. Then create the initial and target configuration
 * 3. Start the game!
 */
const main = () =>
  startFlipBitsGame()
    .then(res => initTargetAndInitialFlipArrays(res))
    .then(() => setInitialFlipArray())
    .then(() => giveAMove())
    .then((message) => {
      console.log(message);
      return Promise.resolve();
    })
    .finally(() => {
      reader.close();
    })
    .catch((err) => {
      console.log(
        "Ohhh! Please report the error to the developer if you didn't intend it to end.",
        { err }
      );
      return Promise.reject(err);
    });

// run the main program to start the game, let's begin
main();
